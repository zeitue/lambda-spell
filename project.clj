(defproject lambda-spell "0.1"
  :description "Lambda-spell is a educational program to help you learn spelling"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [facts/speech-synthesis "1.0.0"]
                 [seesaw "1.4.4"]
                 [marginalia "0.7.1"]]
  :dev-dependencies [[swank-clojure "1.4.3"]
                     [lein-clojars "0.9.1"]]
  :main spell.core
  :aot [spell.core])
