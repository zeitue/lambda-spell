(ns spell.core (:gen-class))

(use '[speech-synthesis.say :as say])
(use '[clojure.java.shell :only [sh]])
(use 'seesaw.core)
(use 'seesaw.dev)
(use 'seesaw.chooser)
(use 'clojure.java.io)
(use 'seesaw.keystroke)
(use 'seesaw.keymap)
(use '[clojure.string :only (join split lower-case)])
(use '[marginalia [parser :only (parse-file)]]
     '[clojure.tools [namespace :only (read-file-ns-decl)] [cli :only (cli)]])
(import 'java.io.File)

(native!) ;; for a native looking interface

(defn env [key]
  "Returns the system property for user.<key>"
  (System/getProperty
    (str "user." key)))

(defn festival [x]
  (sh "sh" "-c"
      (str "echo " x " | festival --tts")))

(defn espeak [x]
  (sh "espeak" x))

(defn mac-say [x]
  (sh "say" x))

(defn check-if-installed [x]
  (:exit (sh "sh" "-c" (str "command -v " x
                            " >/dev/null 2>&1 || { echo >&2 \"\"; exit 1; }"))))

(defn exists? [filename]
  (let [f (File. filename)]
    (cond (.exists f) true :else false)))

(defn get-lines [fname]
  (with-open [r (reader fname)]
    (doall (line-seq r))))

(defn load-props [file-name]
  (with-open [^java.io.Reader reader (clojure.java.io/reader file-name)]
    (let [props (java.util.Properties.)]
      (.load props reader)
      (into {} (for [[k v] props] [(keyword k) (read-string v)])))))

(defn make-dir [x]
  (let [path (split x (re-pattern (File/separator))) temp (atom "")]
    (doseq [item path]
      (do (swap! temp str item (File/separator)) (.mkdir (file @temp))))))

(defn GetLists [d]
  (for [f (.listFiles d)]
    (if (.isFile f)
      (.getName f))))

(def SpellingList ())
(def current (atom 0))
(def entry-text (text :id :entry :text "" :focusable? true))
(defn engine-check []
  (def engines
    (conj ["Google"]
          (if (= (check-if-installed "festival") 0) "Festival")
          (if (= (check-if-installed "espeak") 0) "ESpeak")
          (if (= (check-if-installed "say") 0) "Say")))
  (remove nil? engines))
(def TTS (combobox :model (engine-check)))
(defn set-engine [eng] (cond (= eng "Google") (def speak say)
                             (= eng "Festival") (def speak festival)
                             (= eng "ESpeak") (def speak espeak)
                             (= eng "Say") (def speak mac-say)))
(set-engine (str (selection TTS)))

(defn repeat! []
  (let [entries SpellingList]
    (if-not (= (nth entries @current "@_@") "@_@")
      (do (request-focus! entry-text)
          (.start (Thread. #(speak (nth entries @current)))))
      (do (.start (Thread. #(speak "no word to repeat")))
          (alert "No word to repeat")))))

(def correct (atom 0))
(def incorrect (atom 0))
(def CorrL (label :text (str "Correct: " @correct)))
(def IncorrL (label :text (str "Incorrect: " @incorrect)))
(def DirCheck (atom false))
(def ConfigDir
  (str (env "home") (File/separator) ".config" (File/separator) "lambda-spell"))
(def ConfigFile (str ConfigDir (File/separator) "lambda-spell.properties"))
(def ListDir
  (text :text (str (env "home") (File/separator) "Documents"
                   (File/separator) "SpellingLists") :id :tts))


(defn Get-Settings []
  (do
    (text! ListDir (:ListDir (load-props ConfigFile)))
    (selection! TTS (:TTS (load-props ConfigFile)))
    (set-engine (:TTS (load-props ConfigFile)))
    (reset! DirCheck (:DirCheck (load-props ConfigFile)))))

(defn Set-Settings []
  (do
    (if-not (exists? ConfigDir)
      (make-dir ConfigDir))
    (if-not (exists? ConfigFile)
      (.createNewFile (File. ConfigFile)))
    (with-open [wrtr (writer ConfigFile)]
      (.write wrtr (str "ListDir = \"" (text ListDir) "\"\n" "TTS = \""
                        (str (selection TTS)) "\"\nDirCheck = " @DirCheck)))
    (Get-Settings)))

(defn check-ListDir []
  (if-not (exists? (str (text ListDir)))
    (if-not @DirCheck
      (show!
        (pack!
         (dialog :content
                 (flow-panel
                  :items [(text ListDir)
                          (str "does not exist would you like to create it?")])
                 :option-type :yes-no :type :question :success-fn
                 (fn [e] (do (reset! DirCheck true)
                            (Set-Settings)
                            (make-dir (text ListDir))))
                 :no-fn (fn [e] (do (reset! DirCheck true)
                                   (Set-Settings)))))))))

(if (exists? ConfigFile)
  (Get-Settings)
  (Set-Settings))

(defn setup-list [file]
  (request-focus! entry-text)
  (def SpellingList (shuffle (remove #{""}
                                     (get-lines (.getAbsolutePath file)))))
  (let [entries SpellingList]
    (if-not (= (nth entries 0 "@_@") "@_@")
      (speak (str "Spell the word " (first SpellingList)))
      (do (.start (Thread. #(speak "this spelling list contains no words")))
        (alert "This spelling list contains no words"))))
  (reset! current 0)
  (reset! correct 0)
  (reset! incorrect 0)
  (config! CorrL :text (str "Correct: " @correct))
  (config! IncorrL :text (str "Incorrect: " @incorrect)))

(comment (defn SpellWord [word]
           (speak (str "Spell the word " word))
           (def box (read-line))
           (let [sword word]
             (if (= box sword) (speak "correct") (speak "incorrect")))))


(def Preferences-Panel (grid-panel :vgap 20 :hgap 20 :border "Preferences"
                                   :items
                                   ["Spelling Lists" ListDir
                                    "TTS Engines" TTS
                                    (button :text "Save!" :listen
                                            [:action
                                             (fn [e]
                                               (Set-Settings))])]))

(def Preferences-Window
  (frame :title (str (char 0x03BB) "Spell" ": Preferences")
         :icon (icon "spell/icon.png")
         :content Preferences-Panel))

(def Help-Panel
  (grid-panel
   :vgap 4
   :border "Help"
   :items ["With your favorite text editor(Emacs, Vim, Gedit, TextEdit, etc)"
           "make a plain text file containing your spelling words."
           "One word per line, then save your newly created spelling list"
           (str "in " (text ListDir))
           "Now you may open your spellings lists from the File -> Open menu "
           "or by clicking on the list's name in the file -> Lists menu."
           "Note: for list to show up in the File -> Lists menu"
           "you may have to restart the program"]))

(def Help-Window
  (frame :title (str (char 0x03BB) "Spell" ": Help")
         :icon (icon "spell/icon.png")
         :size [400 :by 400]
         :content Help-Panel))

(def About-Panel
  (grid-panel :vgap 1 :hgap 20 :border "About"
              :items [(clojure.java.io/resource "spell/icon64.png")
                      (str (char 0x03BB) "Spell") "0.1"
                      "A program to help you learn to spell"
                      "Copyright © 2012 ice-os.com"]))

(def About-Window
  (frame :title (str (char 0x03BB) "Spell" ": About")
         :icon (icon "spell/icon.png")
         :size [400 :by 350]
         :content About-Panel))

(def Preferences-Action
  (action :handler (fn [e]
                     (show! (pack! Preferences-Window)))
          :name "Preferences"
          :key "menu P"
          :tip "Set Preferences"))

(def About-Action
  (action :handler (fn [e]
                     (show! (pack! About-Window)))
          :name "About"
          :tip "About this program"))

(def Help-Action
  (action :handler (fn [e]
                     (show! (pack! Help-Window)))
          :name "Help"
          :tip "Help and Usage"))

(def Exit-Action
  (action :handler (fn [e]
                     (System/exit 0))
          :name "Exit"
          :key "menu Q"
          :tip "Quit program"))


(def Open-Action
  (action :handler
          (fn [e]
            (choose-file
             :type :open :selection-mode :files-only :dir (text ListDir)
             :success-fn (fn [fc file] (setup-list file))))
          :name "Open"
          :key "menu O"
          :tip "Open spelling list"))

(def Lists
  (remove nil? (GetLists (File. (text ListDir)))))


(defn check-spelling []
  (let [entries SpellingList]
    (if-not (= (nth entries @current "@_@") "@_@")
      (do
        (if (= (lower-case (text entry-text)) (nth entries @current))
          (do
            (swap! correct inc) (config! CorrL :text
                                         (str "Correct: " @correct))
            (speak "correct"))
          (do
            (swap! incorrect inc)
              (config! IncorrL :text (str "Incorrect: " @incorrect))
              (.start (Thread. #(speak "incorrect")))
              (alert (str "Incorrect\n Your spelling: "
                          (lower-case (text entry-text))
                          "\n Correct spelling: " (nth entries @current)))))
        (text! entry-text "")
        (swap! current inc)
        (if-not (= (nth entries @current "@_@") "@_@")
          (.start (Thread. #(speak (str "Spell the word "
                                        (nth entries @current)))))
          (do
            (.start (Thread.
                     #(speak "you have reached the end of your spelling list")))
            (alert "You have reached the end of your spelling list"))))
      (do
        (.start (Thread.
                 #(speak "no spelling list is loaded")))
        (alert "No spelling list is loaded")))))






(def Main-Panel
  (let [next-button
        (button :text (str "Next    (" (char 0x23CE) ")")
                :listen [:action
                         (fn [e]
                           (do
                             (check-spelling)
                             (request-focus! entry-text)))])
        repeat-button
        (button :text
                (str "Repeat! ("
                     (char 0x21E7)
                     (char 0x23CE) ")")
                :listen [:action
                         (fn [e]
                           (do
                             (repeat!)
                             (request-focus! entry-text)))])]
  (grid-panel :vgap 7
              :hgap 20
              :border "Main"
              :items
              [(top-bottom-split
                entry-text
                (left-right-split
                 (grid-panel :vgap 7 :items [CorrL IncorrL])
                 (grid-panel :vgap 7
                             :items
                             [next-button repeat-button])
                 :divider-location 1/3)
                :divider-location 1/3)])))

(defn make-list-menu []
  (vec (for [item Lists]
     (menu-item :text item
                :listen
                [:action (fn [e]
                           (let [file (File.
                                       (str (text ListDir)
                                            (File/separator) item))]
                             (setup-list file)))]))))

(def Main-Window
  (frame
   :title (str (char 0x03BB) "Spell")
   :icon (icon "spell/icon.png")
   :on-close :exit :size [425 :by 172]
   :menubar (menubar
             :items [(menu :text "File"
                           :items [Open-Action (menu :text "Lists"
                                                     :items (make-list-menu))
                                   Exit-Action])
                     (menu :text "Edit"
                           :items [Preferences-Action])
                     (menu :text "Help"
                           :items [Help-Action About-Action])])
   :content Main-Panel))

(map-key Main-Window
         (keystroke "ENTER")
         (fn [e] (check-spelling)))


(map-key Main-Window
         (keystroke "shift ENTER")
         (fn [e] (repeat!)))


(defn -main [& args]
  (do
    (show! Main-Window)
    (check-ListDir)))
